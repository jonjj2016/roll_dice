const express = require("express")
const cors = require ("cors")
require('dotenv').config()
const db = require('./models/index.js')
const http = require("http")
const { Server } = require("socket.io")
const connectSocket = require('./socket.js')

db.sequelize
  .sync({ force: true })
  .then(() => {
    console.log("Database tables have been created.")
  })
  .catch((error) => {
    console.error("Error creating database tables:", error)
  })


const userRouter = require ('./routes/users')
const transactionsRouter = require("./routes/transactions.js")

const app = express()
app.use(cors())

app.use(express.json())

const server = http.createServer(app)
const io = new Server(server, {
  cors: {
    origin: [
      "http://localhost:4020",
    ],
    methods: ["GET", "POST"],
  },
})
const PORT = process.env.PORT || 4000
server.listen(PORT, () => {
  console.log(`Socket running on port ${PORT}`)
})
connectSocket(io)
app.use('/auth', userRouter)
app.use('/transactions', transactionsRouter)

exports.io = io
module.exports = app
