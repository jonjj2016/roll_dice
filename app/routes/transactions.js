const express = require('express')
const JwtStrategy = require('../config/authConfig')
const { createTransaction } = require("../components/transactions")

const router = express.Router()

router.post("/:gameId", JwtStrategy, createTransaction)

module.exports = router