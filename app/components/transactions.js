const asyncHandler = require("express-async-handler")
const { sequelize } = require("../models/index.js")
const { User, Game } = require("../models/index.js")
const app = require("../app.js")

const { Transaction } = require("../models/index.js")


const createTransaction = asyncHandler(async (req, res) => {
  const { amount } = req.body
  const { gameId } = req.params

  let transaction
  const user = await User.findByPk(req.user.id)
  const game = await Game.findByPk(gameId)
  if (amount > user.balance) {
    return res.status(400).json({
      status: false,
      message: "Insufficient funds",
    })
  }
  const newNumbers = []
  for (let i = 0; i < 3; i++) {
    newNumbers.push(Math.floor(Math.random() * 10) + 1)
  }
  const won = newNumbers[0] === newNumbers[1] && newNumbers[1] === newNumbers[2]

  sequelize
    .transaction(async (t) => {
      try {
        if (!won) {
          user.balance -= amount
          game.wallet += amount
        } else {
          user.balance += amount
          game.wallet -= amount
        }
        await user.save()
        await game.save()

        transaction = await Transaction.create({
          user: req.user.id,
          game: gameId,
          amount,
        })
      } catch (error) {
        console.error(error)
        await t.rollback()
      }
    })
    .catch((error) => {
      console.log(error.message)
    })
    .finally(async () => {
      const user = await User.findByPk(req.user.id)
      app.io.emit("balance_change", user.balance)
      return res.status(201).json({
        numbers: newNumbers,
        result: won,
        transaction,
      })
    })
})
module.exports = {
  createTransaction,
}
