const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const asyncHandler = require("express-async-handler")
const { User, Game } = require("../models/index.js")

const register = asyncHandler(async (req, res) => {
  const { username, password, email } = req.body
  const hashedPassword = await bcrypt.hash(password, 10)
  try {
    const user = await User.create({
      username,
      email,
      password: hashedPassword,
    })
    await Game.create({
      name: "Some Game",
      wallet: 100,
    })
    const token = jwt.sign({ id: user.id }, process.env.JWK_SECRET_KEY)
    return res.status(201).json({
      status: true,
      data: {
        token,
        username: user.username,
        email: user.email,
        id: user.id,
        balance: user.balance,
      },
    })
  } catch (error) {
    if (error.name === "SequelizeValidationError") {
      console.log("Validation errors:", error.errors)
    } else {
      console.error("Error creating user:", error)
    }
  }
})

const login = asyncHandler(async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ where: { email } })

  if (!user || !(await bcrypt.compare(password, user.password))) {
    res.status(401)
    throw new Error("Invalid credentials")
  }

  const token = jwt.sign({ id: user.id }, process.env.JWK_SECRET_KEY)
  res.status(200).json({
    status: true,
    data: {
      token,
      id: user.id,
      username: user.username,
      email: user.email,
      balance: user.balance,
    },
  })
})
module.exports = {
  register,
  login,
}