const { User, Game, Transaction, sequelize } = require("./models/index.js")
const jwt = require("jsonwebtoken")

const connectSocket = async (io) => {
  io.on("connection", (socket) => {
    const { token } = socket.handshake.auth
    socket.on("roll_dice", async ({ gameId, amount }) => {
      const { id } = jwt.verify(token, process.env.JWK_SECRET_KEY)
      const user = await User.findByPk(id)
      if (amount > user.balance) {
        return socket.emit("no_funds", {
          message: "Insufficient funds",
        })
      }
      const game = await Game.findByPk(gameId)
      let transaction
      const newNumbers = []
      for (let i = 0; i < 3; i++) {
        newNumbers.push(Math.floor(Math.random() * 10) + 1)
      }

      const won =
        newNumbers[0] === newNumbers[1] && newNumbers[1] === newNumbers[2]

      sequelize
        .transaction(async (t) => {
          try {
            if (!won) {
              user.balance -= amount
              game.wallet += amount
            } else {
              user.balance += amount
              game.wallet -= amount
            }
            await user.save()
            await game.save()

            transaction = await Transaction.create({
              user: id,
              game: gameId,
              amount,
            })
            await t.commit()
          } catch (error) {
            console.error(error)
            await t.rollback()
          }
        })
        .catch((error) => {})
        .finally(async () => {
          const user = await User.findByPk(id)
          socket.emit("balance_change", user.balance)
          socket.emit("roll_result", {
            numbers: newNumbers,
            result: won,
            transaction,
          })
        })
    })
    socket.on("disconnect", () => {
      console.log("Client has disconnected", socket.id)
    })
    socket.on("error", (err) => {
      console.log(err.message)
    })
  })
}
module.exports = connectSocket
