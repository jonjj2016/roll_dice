module.exports= (sequelize, DataTypes) => {
    const Transaction = sequelize.define("Transaction", {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      user: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      game: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true,
          min: 1,
        },
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.Sequelize.NOW,
      },
    })
    
    Transaction.associate = (models) => {
      Transaction.belongsTo(models.User, {
        foreignKey: "userId",
        as: "transactionUser",
      })
      Transaction.belongsTo(models.Game, {
        foreignKey: "gameId",
        as: "transactionGame",
      })
    }
    return Transaction
}