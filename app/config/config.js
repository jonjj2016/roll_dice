module.exports = {
  development: {
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    host: process.env.DB_HOST,
    dialect: "postgres",
    port: process.env.DB_PORT,
  },
  test: {
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    host: process.env.DB_HOST,
    dialect: "postgres",
    port: process.env.DB_PORT,
  },
  production: {
    password: process.env.DB_PASSWORD_PROD,
    database: process.env.DB_PROD,
    host: process.env.DB_HOST_PROD,
    dialect: "postgres",
    port: process.env.DB_PORT_PROD,
  },
}
