const passport = require("passport")
const passportJWT = require("passport-jwt")

const { User } = require('../models')

const { ExtractJwt, Strategy: JwtStrategy } = passportJWT

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWK_SECRET_KEY,
}

const strategy = new JwtStrategy(jwtOptions, async (jwtPayload, next) => {
  const user = await User.findOne({ where: { id: jwtPayload.id } })
  if (!user) {
    return next(null, false)
  }
  return next(null, user)
})

passport.use(strategy)
module.exports = passport.authenticate("jwt", { session: false })
